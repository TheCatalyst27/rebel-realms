from gameGlobals import *
from ruleBook import *
from displayGameBoard import *
from attackPhases import *
from random import *

def checkWinStatus():
	winningCondition = ["", "", "", "", ""]
	
	if(AI_BOARD == winningCondition):
		print("Congratulations, You win the game!")
		gameMenu()
	elif(P1_BOARD == winningCondition):
		print("Unfortunately, you lose the game!")
		gameMenu()

#Rolls 3 dice for both players.
def moralePhase():

	global rollCounter

	checkWinStatus()

	#Rolls player morale dice.
	playerMoraleDieOne = randint(1,6)
	playerMoraleDieTwo = randint(1,6)
	playerMoraleDieThree = randint(1,6)
	
	#Gets total of player dice.
	playerTotalMorale = playerMoraleDieOne + playerMoraleDieTwo + playerMoraleDieThree
	
	#Rolls ai morale dice.
	aiMoraleDieOne = randint(1,6)
	aiMoraleDieTwo = randint(1,6)
	aiMoraleDieThree = randint(1,6)
	
	#Gets total of ai dice.
	aiTotalMorale = aiMoraleDieOne + aiMoraleDieTwo + aiMoraleDieThree
	
	#Shows roll totals.
	print("You rolled: "),
	print(playerTotalMorale)
	print("")
	print("AI rolled: "),
	print(aiTotalMorale)
	print("")
	
	#Dictates who won the phase and starts the according phase.
	if(playerTotalMorale > aiTotalMorale):
		print("You win, it is your turn to attack.")
		print("")
		rollCounter = 0
		playerAttackPhase()
	if(playerTotalMorale < aiTotalMorale):
		print("AI won, it is their turn to attack.")
		print("")
		aiAttackPhase()
	elif(playerTotalMorale == aiTotalMorale):
		print("It's a tie, both of you will attack.")
		print("")
		rollCounter = 0
		tieAttackPhase()

	startGame()

#Starts the game.
def startGame():

    #Only displays game board on new roles.
	global refreshTracker
	while(refreshTracker == 0):
		displayGameBoard()
		refreshTracker += 1
	
	print("")
	print("")
	print("Press 1 to start morale phase.")
	print("")
	
	moraleDice = int(raw_input(""))
	
	print("")
	
	if(moraleDice == 1):
		refreshTracker = 0
		moralePhase()
	elif(moraleDice != 1):
		print("Try again.")
		refreshTracker += 1
		startGame()

#Deals cards, dependant of which team they are.
def dealCards():
	#Sets counters for loops
	playerCardsInDeck = 0
	aiCardsInDeck = 0

	#Deals five cards to player values, checks for available cards.
	while(playerCardsInDeck < 5):
		cardSelection = choice(P1_DECK)
		availableCard = P1_DECK.count(cardSelection)
		
		if(availableCard >= 1):
			P1_VALUES.append(cardSelection)
			P1_DECK.remove(cardSelection)
			playerCardsInDeck += 1
		else:
			pass
		
	#Deals five cards to ai values, checks for available cards.
	while(aiCardsInDeck < 5):
		cardSelection = choice(AI_DECK)
		availableCard = AI_DECK.count(cardSelection)
		
		if(availableCard >= 1):
			AI_VALUES.append(cardSelection)
			AI_DECK.remove(cardSelection)
			aiCardsInDeck += 1
		else:
			pass
			
	startGame()

#Has user pick which team to play.
def teamSelection():

	global P1_TEAM
	global AI_TEAM

	print("Press 1 to play as Shadows.")
	print("Press 2 to play as Zrads.")
	print("")

	pickTeam = int(input(""))

	if(pickTeam == 1):
		P1_TEAM = 1
		AI_TEAM = 2
	elif(pickTeam == 2):
		P1_TEAM = 2
		AI_TEAM = 1
	elif(pickTeam != 1 or 2):
		print("Try again.")
		teamSelection()
	
	print("")

	dealCards()	

#Welcome user and decide whether to play or quit.
def gameMenu():
	print("Welcome to Rebel Realms.")

	print("Press 1 to play Original Battle, press 2 for help, or press 3 to quit.")
	print("")

	menuSelection = int(input(""))

	print("")

	if(menuSelection == 1):
		teamSelection()
	elif(menuSelection == 2):
		displayRules()
		gameMenu()
	elif(menuSelection == 3):
		print("Thank you.")
		return 0
	print("")