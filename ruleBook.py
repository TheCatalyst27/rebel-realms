#Shows the abilities of the power cards for each deck.
def showAbilites():
	print("Shadows:")
	print("Revive - Able to revive any one card from any graveyard.")
	
	print("")
	
	print("Zrads:")
	print("Freeze - Opponent gets frozen for a turn. The morale phase is skipped and the player attacks immediately.")
	
	print("")
	
	displayRules()

#Shows the rules of Original Battle.
def showOriginalBattle():
	print("Original Battle (OB) is the standard play style of 'Battle' involving only 2 plyaers.")
	print("In OB both players will select a deck and shuffle. Both players begin the game with either five or ten health.")
	print("Next, both players will take the first five cards of the deck and lay them face down, horizontally in a line.")
	print("This is known as the 'defense.' Now that the field is set up, it's time to start the phases.")
	
	print("")
	
	print("First, both players will roll three dice, known as the 'morale.' The player with the highest sum of morale will then enter the 'attack' phase.")
	print("During the attack phase, the player will roll two dice, equalling the attack. The player can then choose any of the opponents defense cards, which will then be flipped over.")
	print("If the player's attack is equal or higher than the opponent's defense, then the defense card is destroyed.")
	print("If the player's attack is less than the opponent's defense, then the defense stands and remains face up.")
	print("After the attack phase, both players re-enter the morale phase. The game continues until one player attacks through their oppoonent's defense and attacks the player directly.")
	print("When a player is attacked directly, they lose one health and the game is reset. Players will collect all of their cards, re-shuffle them, and lay down their new defense.")
	print("The game will continue and only end once on player has lost all of their health or forfeits the game.")
		
	print("")
	
	displayRules()

#Shows the rules of play.
def showRules():
	print("The game is played with a minimum of two player, each with twenty-two cards and two dice.")
	print("Each deck is similar and consists of two basic cards, valued 1 through ten,")
	print("Two value cards valued at eleven, and two power cards that have a unique ability.")
	
	print("")
	
	print("Dice:")
	print("During the morale phase, players roll three dice.")
	print("Doubles: If a player rolls two of the same dice, then they have the option to flip all face up cards down and shuffle their defense.")
	print("Triples: If a player rolls three of the same dice, then they may take the next card in their deck and add it to their defense.")
	print("Equal Value 'Morale': If both players roll the same value durnign the morale phae, then both players roll the attack dice and the higher value attacks.")
	print("Equal Value 'Attack': If both players roll the same value during the atttack phase, then no players attack and replay the morale phase.")
	
	print("")
	
	print("Forfeits: Any player may forfeit the round at any time, but in return loses 2 health.")
	
	print("")
	
	displayRules()

#Shows help text.
def displayRules():

	#Controls that this is only shown once and not every time it's called.
	menuCounter = 0
	
	while(menuCounter < 1):
		print("Rebel Realms Rules:")
		print("The object of the game is simple, deplete your opponents' health.")
		print("The challenge comes from your skill of dice and how your luck of cards play together.")
		menuCounter += 1

	print("")
	
	print("Press 1 for Rules, 2 for Original Battle, or 3 for Abilities.")
	
	print("")
	
	ruleSelection = int(input(""))
	
	print("")

	if(ruleSelection == 1):
		showRules()
	if(ruleSelection == 2):
		showOriginalBattle()
	if(ruleSelection == 3):
		showAbilites()
	else:
		return