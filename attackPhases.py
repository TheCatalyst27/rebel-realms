from gameGlobals import *
from displayGameBoard import *
from random import randint

def playerAttackPhase():
	
	global rollCounter
	
	while(rollCounter == 0):
		print("Press 1 to roll your attack dice.")
		print("")
	
		attackDice = int(input(""))
	
		#Rolls player attack dice.
		if(attackDice == 1):
			playerAttackDieOne = randint(1,6)
			playerAttackDieTwo = randint(1,6)
		else:
			playerAttackPhase()
	
		#Gets total of player dice.
		playerTotalAttack = playerAttackDieOne + playerAttackDieTwo
		rollCounter += 1
	
		print("")
		print("You rolled: "),
		print(playerTotalAttack)
	
	print("")
	displayGameBoard()
	print("")
	print("Select a card to attack.")
	print("")
	
	#Selects which defense position to attack.
	defensePosition = int(input(""))
	
	if(defensePosition == 1):
		#Sets AI value to display during attack.
		AI_BOARD[0] = AI_VALUES[0]
		
		#Converts strings to ints for comparison.
		aiDefenseValue = int(AI_BOARD[0])
		
		if(playerTotalAttack >= aiDefenseValue):
			AI_BOARD[0] = ""
		else:
			pass
			
		rollCounter = 0
		return
		
	elif(defensePosition == 2):
		#Sets AI value to display during attack.
		AI_BOARD[1] = AI_VALUES[1]
		
		#Converts strings to ints for comparison.
		aiDefenseValue = int(AI_BOARD[1])
		
		if(playerTotalAttack >= aiDefenseValue):
			AI_BOARD[1] = ""
		else:
			pass
		
		rollCounter = 0
		return
	
	elif(defensePosition == 3):
		#Sets AI value to display during attack.
		AI_BOARD[2] = AI_VALUES[2]
		
		#Converts strings to ints for comparison.
		aiDefenseValue = int(AI_BOARD[2])
		
		if(playerTotalAttack >= aiDefenseValue):
			AI_BOARD[2] = ""
		else:
			pass
			
		rollCounter = 0
		return
		
	elif(defensePosition == 4):
		#Sets AI value to display during attack.
		AI_BOARD[3] = AI_VALUES[3]
		
		#Converts strings to ints for comparison.
		aiDefenseValue = int(AI_BOARD[3])
		
		if(playerTotalAttack >= aiDefenseValue):
			AI_BOARD[3] = ""
		else:
			pass
			
		rollCounter = 0
		return
		
	elif(defensePosition == 5):
		#Sets AI value to display during attack.
		AI_BOARD[4] = AI_VALUES[4]
		
		#Converts strings to ints for comparison.
		aiDefenseValue = int(AI_BOARD[4])
		
		if(playerTotalAttack >= aiDefenseValue):
			AI_BOARD[4] = ""
		else:
			pass
		
		rollCounter = 0
		return
		
	elif(defensePosition != 1 or 2 or 3 or 4 or 5):
		print("Invalid option, try again.")
		rollCounter += 1
		
def aiAttackPhase():
	
	#Rolls ai attack dice.
	aiAttackDieOne = randint(1,6)
	aiAttackDieTwo = randint(1,6)
	
	#Gets total of ai dice.
	aiTotalAttack = aiAttackDieOne + aiAttackDieTwo
	
	print("AI rolled: "),
	print(aiTotalAttack)
	print("")
	displayGameBoard()
	
	#Selects which defense position to attack.
	aiDefensePosition = randint(1,5)
	
	if(aiDefensePosition == 1):
		#Sets player value to display during attack.
		P1_BOARD[0] = P1_VALUES[0]
		
		#Converts strings to ints for comparison.
		playerDefenseValue = int(P1_BOARD[0])
		
		if(aiTotalAttack >= playerDefenseValue):
			P1_BOARD[0] = ""
		else:
			pass
		
		return
		
	elif(aiDefensePosition == 2):
		#Sets player value to display during attack.
		P1_BOARD[1] = P1_VALUES[1]
		
		#Converts strings to ints for comparison.
		playerDefenseValue = int(P1_BOARD[1])
		
		if(aiTotalAttack >= playerDefenseValue):
			P1_BOARD[1] = ""
		else:
			pass
		
		return
		
	elif(aiDefensePosition == 3):
		#Sets player value to display during attack.
		P1_BOARD[2] = P1_VALUES[2]
		
		#Converts strings to ints for comparison.
		playerDefenseValue = int(P1_BOARD[2])
		
		if(aiTotalAttack >= playerDefenseValue):
			P1_BOARD[2] = ""
		else:
			pass
		
		return
		
	elif(aiDefensePosition == 4):
		#Sets player value to display during attack.
		P1_BOARD[3] = P1_VALUES[3]
		
		#Converts strings to ints for comparison.
		playerDefenseValue = int(P1_BOARD[3])
		
		if(aiTotalAttack >= playerDefenseValue):
			P1_BOARD[3] = ""
		else:
			pass
		
		return
		
	elif(aiDefensePosition == 5):
		#Sets player value to display during attack.
		P1_BOARD[4] = P1_VALUES[4]
		
		#Converts strings to ints for comparison.
		playerDefenseValue = int(P1_BOARD[4])
		
		if(aiTotalAttack >= playerDefenseValue):
			P1_BOARD[4] = ""
		else:
			pass
		
		return
		
	else:
		return

def tieAttackPhase():

	global rollCounter
	
	while(rollCounter == 0):
		print("Press 1 to roll your attack dice.")
		print("")
	
		attackTieDice = int(input(""))
	
		#Rolls player attack dice.
		if(attackTieDice == 1):
			playerAttackTieDieOne = randint(1,6)
			playerAttackTieDieTwo = randint(1,6)
		else:
			tieAttackPhase()
	
		#Gets total of player dice.
		playerTotalTieAttack = playerAttackTieDieOne + playerAttackTieDieTwo
		rollCounter += 1
		
	print("")
	print("You rolled: "),
	print(playerTotalTieAttack)
	print("")
		
	#Rolls ai attack dice.
	aiAttackTieDieOne = randint(1,6)
	aiAttackTieDieTwo = randint(1,6)
	
	#Gets total of ai dice.
	aiTotalTieAttack = aiAttackTieDieOne + aiAttackTieDieTwo
	
	print("AI rolled: "),
	print(aiTotalTieAttack)
	print("")
		
	if(playerTotalTieAttack > aiTotalTieAttack):
		print("You win.")
		print("")
		rollCounter = 0
		playerAttackPhase()
		
	elif(playerTotalTieAttack < aiTotalTieAttack):
		print("You lose.")
		print("")
		aiAttackPhase()
	
	else:
		print("It is a tie.")
		return