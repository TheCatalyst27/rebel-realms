#Team selection variables.
P1_TEAM = 0
AI_TEAM = 0 

#Deck for both teams.
P1_DECK = ["0", "0", "2", "2", "3", "3", "4", "4", "5", "5", "6", "6", "7", "7", "8", "8", "9", "9", "10", "10", "11", "11"]
AI_DECK = ["0", "0", "2", "2", "3", "3", "4", "4", "5", "5", "6", "6", "7", "7", "8", "8", "9", "9", "10", "10", "11", "11"]

#Values of each teams defense.
P1_VALUES = []
AI_VALUES = []

#Tracker to prevent clutter with displaying game board.
refreshTracker = 0

#The game board.
AI_BOARD = ["1", "2", "3", "4", "5"]
P1_BOARD = ["1", "2", "3", "4", "5"]

#Roll counter to prevent multiple rolls.
rollCounter = 0